<?php


namespace Winter\Main\Controllers;


use Bitrix\Main\Application;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Engine\Controller;
use Winter\Main\ActionFilter\BitrixCaptcha;
use Winter\Main\I18n;

class Test extends Controller
{
    public function configureActions()
    {
        return [
            'ping' => [
                'prefilters' => [
                    new HttpMethod([HttpMethod::METHOD_POST]),
                    new Csrf(),
                ],
                'postfilters' => [],
            ],
            'checkCaptcha' => [
                'prefilters' => [
                    new HttpMethod([HttpMethod::METHOD_POST]),
                    new Csrf(),
                    new BitrixCaptcha(),
                ],
                'postfilters' => [],
            ],
        ];
    }

    public function pingAction() {
        return [
            'message' => 'test',
        ];
    }

    public function checkCaptchaAction()
    {
        return [
            'method' => __METHOD__,
        ];
    }
}