<?php


namespace Winter\Main\ActionFilter;


use Bitrix\Main\Application;
use Bitrix\Main\Engine\ActionFilter\Base;
use Bitrix\Main\Error;
use Bitrix\Main\Event;
use Bitrix\Main\EventResult;

class BitrixCaptcha extends Base
{
    const WRONG_CAPTCHA = 'wrong_captcha';

    public function onBeforeAction(Event $event)
    {
        $request = Application::getInstance()->getContext()->getRequest();

        $word = $request->get('captcha_word');
        $code = $request->get('captcha_code');

        global $APPLICATION;

        if(empty($word) || empty($code) || !$APPLICATION->CaptchaCheckCode($word, $code)) {
            $this->errorCollection[] = new Error(
                'wrong_captcha',
                self::WRONG_CAPTCHA
            );

            return new EventResult(EventResult::ERROR, null, null, $this);
        }

        return null;
    }
}