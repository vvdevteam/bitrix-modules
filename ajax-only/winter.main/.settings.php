<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Winter\\Main\\Controllers' => 'api',
            ],
        ],
        'readonly' => true,
    ],
];
