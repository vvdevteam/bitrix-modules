<?php

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

class winter_main extends \CModule
{
    const MODULE_ID = 'winter.main';

    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = self::MODULE_ID;
        $this->MODULE_NAME = 'winter main';
        $this->MODULE_DESCRIPTION = 'MODULE_DESCRIPTION';
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = 'PARTNER_NAME';
        $this->PARTNER_URI = 'PARTNER_URI';
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        if (Loader::includeModule($this->MODULE_ID))
        {
            // Install something
        }
    }

    public function doUninstall()
    {
        if (Loader::includeModule($this->MODULE_ID))
        {
            // Uninstall something
            ModuleManager::unregisterModule($this->MODULE_ID);
        }
    }
}
