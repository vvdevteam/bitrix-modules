<?php


namespace Winter\Main\Reporters;


use Winter\Main\Interfaces\ResultInterface;

interface ReporterInterface
{
    public function send(): ResultInterface;
}