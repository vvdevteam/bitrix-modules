<?php


namespace Winter\Main\Reporters;


use Winter\Main\Analyzes\TextCleaner;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Notifications\PreModerationReportNotification;

class CensorshipReporter implements ReporterInterface
{

    private $cleaner;
    private $text;

    public function __construct()
    {
        $this->cleaner = new TextCleaner();
        $this->text = '';
    }

    public function toCensor(string $text)
    {
        $this->text .= $text . "\n";
        return $this->cleaner->clean($text);
    }

    public function send(): ResultInterface
    {
        $result = $this->cleaner->getResult();
        return (new PreModerationReportNotification($this->text, $result))->send();
    }
}