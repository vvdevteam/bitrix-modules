<?php


namespace Winter\Main\Validators;


use Bitrix\Main\HttpRequest;
use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Result;
use Winter\Main\Services\CarElementInformant;

class CarValidator implements ValidatorInterface
{
    const CHANNEL = 'validation';

    /**
     * @var HttpRequest
     */
    private $request;
    /**
     * @var CarElementInformant
     */
    private $carElementInformant;

    public function __construct(HttpRequest $request)
    {
        $this->carElementInformant = new CarElementInformant();
        $this->request = $request;
    }



    public function validate(array $keysList): ResultInterface
    {
        $parcel = $this->request->getValues();

        /*
        * location: qwdqwd  2
car_features: check1,check5,check9
year: 4324
make: dadwq
model: qwd
odometr: qwdq
plate: qwd
state: qwdqwd
car: qwdqwd
transmission: automatic
vehicle: wqdqdw
fuel_type: 3
seats: 3
doors: 3
fuel_consumption: qdwdqwd
char_text: qwdqwdqwdqwdqwd
notice: 3
min_trip_duration: 3
max_trip_duration: 3
        */

        $emptyKeys = [];
        foreach ($keysList as $key) {
            if (isset($parcel[$key])) {
                $parcel[$key] = trim($parcel[$key]);
            }
            if (!isset($parcel[$key]) || empty($parcel[$key])) {
                $emptyKeys[] = $key;
            }
        }
        
        if (!empty($emptyKeys)) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_empty_fields', ['#KEYS#' => $emptyKeys]))
                ->addMessage(implode(' ', $emptyKeys))
            ;
        }

        //check if exist
        $parcel['location'];

        //check if exist
        $parcel['validation_features'];

        $parcel['year'] = intval($parcel['year']);
        if ($parcel['year'] < 1900 || $parcel['year'] > date('Y')) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_year'));
        }

        $parcel['make'];


        $parcel['model'];


        $parcel['odometr'];

        if ($this->carElementInformant->isPlateAlreadyExist($parcel['plate'])) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_plate_already_exist'));
        }

        $parcel['state'];

        $parcel['car'];

        //check if exist
        $parcel['transmission'] = (int) $parcel['transmission'];

        if (!$this->carElementInformant->isEnum('TRANSMISSION', $parcel['transmission'])) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_transmission'));
        }

        //check if exist
        $parcel['vehicle'] = (int) $parcel['vehicle'];
        if ($parcel['vehicle'] <= 0) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_vehicle'));
        }

        //check if exist
        $parcel['fuel_type'];

        $parcel['seats'] = intval($parcel['seats']);
        if ($parcel['seats'] <= 0) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_seats'));
        }

        $parcel['doors'] = intval($parcel['doors']);
        if ($parcel['doors'] <= 0) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_doors'));
        }

        $parcel['fuel_consumption'];

        $parcel['char_text'];

        $parcel['notice'];

        if ($parcel['min_trip_duration_req']) {
            $parcel['min_trip_duration'] = intval($parcel['min_trip_duration']);
            if ($parcel['min_trip_duration'] <= 0) {
                return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_min_trip_duration'));
            }
        }

        if ($parcel['max_trip_duration_req']) {
            $parcel['max_trip_duration'] = intval($parcel['max_trip_duration']);
            if ($parcel['max_trip_duration'] <= 0) {
                return Result::error(I18n::getMessage(self::CHANNEL, 'validation_invalid_max_trip_duration'));
            }
        }
        
        
        return Result::success(I18n::getMessage(self::CHANNEL, 'validation_success'));
    }

}