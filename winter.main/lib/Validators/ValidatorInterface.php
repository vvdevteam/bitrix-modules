<?php


namespace Winter\Main\Validators;


use Winter\Main\Interfaces\ResultInterface;

interface ValidatorInterface
{
    public function validate(array $keysList): ResultInterface;
}