<?php


namespace Winter\Main\Localization;


interface I18nInterface
{
    public static function getMessage($group, $code, array $replace=null): string;
}