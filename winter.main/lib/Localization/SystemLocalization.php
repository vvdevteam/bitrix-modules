<?php


namespace Winter\Main\Localization;


class SystemLocalization extends BitrixLocalization implements I18nInterface
{
    protected const LANG = 'system';
}