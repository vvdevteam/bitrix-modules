<?php


namespace Winter\Main\Localization;


use Winter\Main\Entities\LocalizationTable;

class DBLocalization implements I18nInterface
{
    private static $loadedGroups = [];

    private static $messages = [];

    private static function loadGroup($code)
    {
        if (!isset(self::$loadedGroups[$code])) {
            $elements = LocalizationTable::query()
                ->where('type', $code)
                ->where('language', CUSTOM_LANGUAGE)
                ->addSelect('value')
                ->addSelect('code')
                ->exec()
                ->fetchAll()
            ;
            if (!empty($elements)) {
                self::$messages += array_column($elements, 'value', 'code');
            }
            self::$loadedGroups[$code] = true;
        }
    }

    public static function getMessage($group, $code, array $replace = null): string
    {
        self::loadGroup($group);
        return  self::$messages[$code] ?? $group.':'.$code;
    }
}