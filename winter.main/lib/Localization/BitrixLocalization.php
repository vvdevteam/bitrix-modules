<?php


namespace Winter\Main\Localization;


use Bitrix\Main\Localization\Loc;

class BitrixLocalization implements I18nInterface
{
    protected const LANG = CUSTOM_LANGUAGE;

    protected static $loadedGroups = [];

    protected static function loadGroup($code)
    {
        if (!isset(self::$loadedGroups[$code])) {
            $locFile = $_SERVER['DOCUMENT_ROOT'].'/local/'.$code.'.php';
            Loc::loadLanguageFile($locFile, static::LANG);
            self::$loadedGroups[$code] = true;
        }
    }

    public static function getMessage($group, $code, array $replace=null): string
    {
        self::loadGroup($group);
        return (string) Loc::getMessage($code, $replace, static::LANG);
    }
}