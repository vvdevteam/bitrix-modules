<?php


namespace Winter\Main\UI\Admin;


use Winter\Main\Entities\LocalizationTable;

class IBlockSectionBitrixUI
{
    private $iBlockId;

    public function __construct(int $iBlockId)
    {
        $this->iBlockId = $iBlockId;
    }

    public function getParameters()
    {
        return [
            "TABSET" => "CUSTOM",
            "GetTabs" => [$this, "tabs"],
            "ShowTab" => [$this, "showtab"],
            "Action" => [$this, "action"],
            "Check" => [$this, "check"],
        ];
    }
    public function action($params) {
        return true;
    }

    public function check($params) {
        return true;
    }

    public function tabs($arArgs) {

        return [
            [
                "DIV" => "localization_edit1",
                "TAB" => "Локализация",
                "ICON" => "sale",
                "TITLE" => "Локализация",
                "SORT" => 100
            ]
        ];
    }

    public function showtab($divName, $params, $bVarsFromForm) {
        $id = $params['ID'];
        $type = "sections:list";
        $elements = LocalizationTable::query()
            ->where('type', $type)
            ->addSelect('code')
            ->addSelect('value')
            ->addSelect('language')
            ->exec()
            ->fetchAll() ?: [];
        $elements = array_map(function ($element) {
            $element['external_id'] = $element['code'] . $element['language'];
            return $element;
        }, $elements);
        $elements = array_column($elements, 'value', 'external_id');

        if ($divName == "localization_edit1") {
            foreach (LANGS_LIST as $lang) {
                if (DEFAULT_LANGUAGE == $lang) {
                    continue;
                }
            ?>

            <tr>
                <td>
                    Название <?=$lang?>
                </td>
                <td>
                    <input type="text" name="NAME_<?=strtoupper($lang)?>" value="<?=$elements["sections:$id:NAME$lang"] ?? ''?>">
                </td>
            </tr>
            <?
            }
        }
    }
}