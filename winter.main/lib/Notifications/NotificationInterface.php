<?php


namespace Winter\Main\Notifications;


use Winter\Main\Interfaces\ResultInterface;

interface NotificationInterface
{
    public function send(): ResultInterface;
}