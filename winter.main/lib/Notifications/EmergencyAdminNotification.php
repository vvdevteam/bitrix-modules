<?php


namespace Winter\Main\Notifications;


use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Messengers\BitrixAdminInformMessenger;
use Winter\Main\Messengers\BitrixEmailMessenger;
use Winter\Main\Messengers\MessengerCollection;
use Winter\Main\Result;

class EmergencyAdminNotification implements NotificationInterface
{
    const CHANNEL = 'notification';

    private $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function send(): ResultInterface
    {
        if (!$this->message) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'notification_fail'));
        }

        return (new MessengerCollection())
            ->addMessenger(new BitrixEmailMessenger('email', 'EMERGENCY_NOTIFY_ADMIN', ['MESSAGE' => $this->message]))
            ->addMessenger(new BitrixAdminInformMessenger($this->message))
            ->setType(static::class)
            ->setEntityId(0)
            ->setTargetId(0)
            ->send()
            ;
    }
}