<?php


namespace Winter\Main\Events\Extensions;


use Winter\Main\Entities\LocalizationTable;

class IblockElement
{
    public static function  onAfterUpdate(&$arFields)
    {
        self::saveLocalization($arFields);
    }

    public static function  onAfterAdd(&$arFields)
    {
        self::saveLocalization($arFields);
    }

    private static function saveLocalization($arFields)
    {
        $locFields = [
            'NAME',
            'DETAIL_TEXT',
        ];
        LANGS_LIST;
        $id = $arFields['ID'];
        $data = [
            'type' => "elements:$id",
            'code' => "elements:$id:NAME",
        ];
        $elements = LocalizationTable::query()
            ->where('type', $data['type'])
            ->addSelect('id')
            ->addSelect('code')
            ->addSelect('language')
            ->exec()
            ->fetchAll()
        ;
        $elements = array_map(function ($element) {
            $element['external_id'] = $element['code'] . $element['language'];
            return $element;
        }, $elements ?? []);
        $elements = array_column($elements, 'id', 'external_id');
        foreach ($locFields as $locField) {
            $data['code'] = "elements:$id:$locField";
            foreach (LANGS_LIST as $lang) {
                $field = "elements:$id:$locField$lang";
                $data['value'] = $_POST[$locField . "_" . strtoupper($lang)] ?? '';
                $data['language'] = $lang;
                if (DEFAULT_LANGUAGE == $lang) {
                    $data['value'] = $arFields[$locField];
                }
                if (isset($elements[$field])) {
                    LocalizationTable::update($elements[$field], ['value' => $data['value']]);
                } else {
                    LocalizationTable::add($data);
                }
            }
        }
    }
}