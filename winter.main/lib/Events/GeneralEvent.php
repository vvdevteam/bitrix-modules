<?php


namespace Winter\Main\Events;


use Winter\Main\Events\Extensions\IblockElement;
use Winter\Main\Events\Extensions\IblockSection;
use Winter\Main\UI\Admin\IBlockElementBitrixUI;
use Winter\Main\UI\Admin\IBlockSectionBitrixUI;

class GeneralEvent
{
    public function iblockOnAfterIBlockElementUpdate(&$arFields)
    {
        IblockElement::onAfterUpdate($arFields);
    }

    public function iblockOnAfterIBlockSectionUpdate($arFields)
    {
        IblockSection::onAfterUpdate($arFields);
    }

    public static function iblockOnAfterIBlockElementAdd($arFields)
    {
        IblockElement::onAfterAdd($arFields);
    }

    public function iblockOnAfterIBlockSectionAdd($arFields)
    {
        IblockSection::onAfterAdd($arFields);
    }

    public static function mainOnAdminIBlockElementEdit($params)
    {
        return (new IBlockElementBitrixUI((int) $params['IBLOCK']['ID']))->getParameters();
    }

    public static function mainOnAdminIBlockSectionEdit($params)
    {
        return (new IBlockSectionBitrixUI((int) $params['IBLOCK']['ID']))->getParameters();
    }

    public static function saleOnOrderUpdate()
    {

    }
}