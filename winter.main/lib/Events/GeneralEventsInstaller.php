<?php


namespace Winter\Main\Events;


class GeneralEventsInstaller extends AbstractEventInstaller
{
    protected static function events(): array
    {
        return [
            [
                'sale',//змінна
                'OnOrderUpdate',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'saleOnOrderUpdate'//тільки метод класу GeneralEvent
            ],
            [
                'main',//змінна
                'OnAdminIBlockElementEdit',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'mainOnAdminIBlockElementEdit'//тільки метод класу GeneralEvent
            ],
            [
                'main',//змінна
                'OnAdminIBlockSectionEdit',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'mainOnAdminIBlockSectionEdit'//тільки метод класу GeneralEvent
            ],
            [
                'iblock',//змінна
                'OnAfterIBlockElementUpdate',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'iblockOnAfterIBlockElementUpdate'//тільки метод класу GeneralEvent
            ],
            [
                'iblock',//змінна
                'OnAfterIBlockSectionUpdate',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'iblockOnAfterIBlockSectionUpdate'//тільки метод класу GeneralEvent
            ],
            [
                'iblock',//змінна
                'OnAfterIBlockElementAdd',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'iblockOnAfterIBlockElementAdd'//тільки метод класу GeneralEvent
            ],
            [
                'iblock',//змінна
                'OnAfterIBlockSectionAdd',//змінна
                static::MODULE_ID,//постійна
                GeneralEvent::class,//постійна
                'iblockOnAfterIBlockSectionAdd'//тільки метод класу GeneralEvent
            ],
        ];
    }
}