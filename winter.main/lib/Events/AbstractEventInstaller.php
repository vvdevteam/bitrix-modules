<?php


namespace Winter\Main\Events;


use Bitrix\Main\EventManager;

abstract class AbstractEventInstaller
{
    protected const MODULE_ID = 'winter.main';

    protected abstract static function events(): array;

    public static function install()
    {
        foreach (static::events() as $event)
        {
            $dbEvents = EventManager::getInstance()
                ->findEventHandlers($event[0], $event[1], [$event[2]])
            ;

            foreach($dbEvents as $dbEvent)
            {
                if ($event[3] === $dbEvent['TO_CLASS'] && $event[4] === $dbEvent['TO_METHOD']) {
                    break;
                }
            }

            EventManager::getInstance()
                ->registerEventHandler(...$event)
            ;
        }
    }

    public static function uninstall()
    {
        foreach (static::events() as $event)
        {
            EventManager::getInstance()
                ->unRegisterEventHandler(...$event)
            ;
        }
    }
}