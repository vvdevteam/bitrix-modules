<?php


namespace Winter\Main\Messengers;


interface MessengerWithContext extends MessengerInterface, ContextInterface
{

}