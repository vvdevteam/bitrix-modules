<?php


namespace Winter\Main\Messengers;


use Winter\Main\Interfaces\ResultInterface;

interface MessengerInterface
{
    public function send(): ResultInterface;
}