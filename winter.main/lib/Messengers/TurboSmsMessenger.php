<?php


namespace Winter\Main\Messengers;


use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Logger;
use Winter\Main\Messengers\Entities\SentMessage;
use Winter\Main\Messengers\Settings\TurboSmsSettings;
use Winter\Main\Result;
use Winter\Main\Services\AuthorizedHttpClient;

class TurboSmsMessenger implements MessengerWithContext
{
    const CHANNEL = 'messenger';

    private $settings;
    private $client;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     */
    private $text;

    public function __construct(string $phone, string $text)
    {
        $this->settings = new TurboSmsSettings();
        $this->client = new AuthorizedHttpClient($this->settings->getToken());
        $this->phone = $phone;
        $this->text = $text;
    }

    public function getRecipient(): string
    {
        return $this->phone;
    }

    public function getContent(): string
    {
        return $this->text;
    }

    public function initSend()
    {
        if (!$this->client->isReady()) {
            return [
                'response_status' => 'BITRIX_EMPTY_TOKEN',
            ];
        }
        if (!$this->settings->getSmsSender()) {
            return [
                'response_status' => 'BITRIX_EMPTY_SENDER',
            ];
        }
        $data = [
            "recipients" => [$this->phone],
            "sms" => [
                "sender" => $this->settings->getSmsSender(),
                "text" => $this->text,
            ]
        ];

        return $this->client->post(TurboSmsSettings::SEND_MESSAGE, $data);
    }

    public function send(): ResultInterface
    {
        $smsResult = $this->initSend();

        if (!is_array($smsResult)) {

            Logger::error(self::CHANNEL, compact('recipientId', 'message', 'smsResult'));

            return Result::error(I18n::getMessage(self::CHANNEL, 'messenger_sms_send_error'))
                ->addTags('turbo_sms')
                ->addTags('sms_send')
            ;
        }

        Logger::debug(self::CHANNEL, $smsResult);

        $responseStatus = $smsResult['response_status'] ?? '';


        list($smsMessage) = $smsResult['response_result'] ?? [[]];
        $smsStatus = $smsMessage['response_status'] ?? '';
        $smsId = $smsMessage['message_id'] ?? '';
        if ($this->settings->isSuccessfulResponseStatus($responseStatus)) {
            if (!$this->settings->isSuccessfulSmsStatus($smsStatus)) {
                Logger::error(self::CHANNEL, compact('recipientId', 'message', 'smsStatus'));

                return Result::error(I18n::getMessage(self::CHANNEL, 'messenger_sms_send_error'))
                    ->addTags('turbo_sms')
                    ->addTags('sms_send')
                ;
            } else {
                return Result::success()
                    ->set('id', $smsId)
                    ->addTags('turbo_sms')
                    ->addTags('sms_send')
                    ->addTags('has_id')
                ;
            }
        }

        Logger::error(self::CHANNEL, compact('recipientId', 'message', 'responseStatus'));

        return Result::error($responseStatus)
            ->addTags('turbo_sms')
            ->addTags('sms_send')
        ;
    }
}