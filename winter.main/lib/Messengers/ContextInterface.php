<?php


namespace Winter\Main\Messengers;


interface ContextInterface
{
    public function getRecipient(): string;
    public function getContent(): string;
}