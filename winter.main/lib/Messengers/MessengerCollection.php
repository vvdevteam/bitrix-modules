<?php


namespace Winter\Main\Messengers;


use Winter\Main\Entities\SentMessagesTable;
use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Logger;
use Winter\Main\Result;

class MessengerCollection implements MessengerInterface
{
    const CHANNEL = 'messenger';

    private $messengers;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $entity_id;
    /**
     * @var string
     */
    private $target_id;

    public function __construct()
    {
        $this->messengers = [];

        $this->type = '';
        $this->entity_id = '';
        $this->target_id = '';
    }

    public function setType($type): self
    {
        $this->type = $type;
        return $this;
    }

    public function setEntityId($entity_id): self
    {
        $this->entity_id = $entity_id;

        return $this;
    }

    public function setTargetId($target_id): self
    {
        $this->target_id = $target_id;

        return $this;
    }

    public function addMessenger(MessengerInterface $messenger)
    {
        $this->messengers[] = $messenger;

        return $this;
    }

    private function saveResult(ResultInterface $result, ContextInterface $messenger)
    {
        if (!$result->isSuccess()) {
            return;
        }
        if (empty($result->getTags())) {
            return;
        }
        if (!in_array('has_id', $result->getTags())) {
            return;
        }

        $data = [
            "external_id" => $result->get('id'),
            "type" => $this->type,
            "messenger" => \get_class($messenger),
            "entity_id" => $this->entity_id,
            "target_id" => $this->target_id,
            "recipient" => $messenger->getRecipient(),
            "language" => DEFAULT_LANGUAGE,
            "content" => $messenger->getContent(),
        ];

        $result = SentMessagesTable::add($data);

        if (!$result->isSuccess()) {
            Logger::error(self::CHANNEL, $result->getErrorMessages(), 'SentMessagesTable::add');
        }
    }

    public function send(): ResultInterface
    {
        $result = Result::error();

        if (empty($this->messengers)) {
            return $result
                ->addTags('empty')
            ;
        }

        /**
         * @var MessengerWithContext $messenger
         */
        foreach ($this->messengers as $messenger) {
            $result = $messenger->send()
                ->addTags(\get_class($messenger))
            ;
            if ($result->isSuccess()) {
                $this->saveResult($result, $messenger);
                return $result;
            } else {
                Logger::error(self::CHANNEL, $result->toArray(), 'MessengerInterface::send');
            }
        }

        return $result;
    }
}