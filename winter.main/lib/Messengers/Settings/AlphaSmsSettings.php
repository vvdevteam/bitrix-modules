<?php


namespace Winter\Main\Messengers\Settings;


use Bitrix\Main\Config\Option;

class AlphaSmsSettings
{
    const MODULE = 'winter.main';

    const ENDPOINT = 'https://alphasms.ua/api/http.php';

    private $token;
    private $smsSender;

    public function __construct()
    {
        $this->token = Option::get(static::MODULE, 'ALPHA_SMS_TOKEN', '');
        $this->smsSender = Option::get(static::MODULE, 'ALPHA_SMS_SMS_SENDER', '');
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getSmsSender(): string
    {
        return $this->smsSender;
    }
}