<?php


namespace Winter\Main\Messengers\Settings;


use Bitrix\Main\Config\Option;

class TurboSmsSettings
{
    const MODULE = 'winter.main';

    const SUCCESS_MESSAGE_SEND = ['OK', 'SUCCESS_MESSAGE_SENT'];
    const SUCCESS_MESSAGE_ADD_IN_QUEUE = ['SUCCESS_MESSAGE_ACCEPTED', 'SUCCESS_MESSAGE_SENT'];

    const USER_BALANCE = 'https://api.turbosms.ua/user/balance.json';
    const SEND_MESSAGE = 'https://api.turbosms.ua/message/send.json';
    const MESSAGE_STATUS = 'https://api.turbosms.ua/message/status.json';

    private $token;
    private $smsSender;

    public function __construct()
    {
        $this->token = Option::get(static::MODULE, 'TURBO_SMS_TOKEN', '');
        $this->smsSender = Option::get(static::MODULE, 'TURBO_SMS_SMS_SENDER', '');
    }

    public function isSuccessfulResponseStatus($status): bool
    {
        return in_array($status, TurboSmsSettings::SUCCESS_MESSAGE_ADD_IN_QUEUE);
    }

    public function isSuccessfulSmsStatus($status): bool
    {
        return in_array($status, TurboSmsSettings::SUCCESS_MESSAGE_SEND);
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getSmsSender(): string
    {
        return $this->smsSender;
    }
}