<?php


namespace Winter\Main\Messengers;


use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Result;

class BitrixAdminInformMessenger implements MessengerWithContext
{
    const CHANNEL = 'messenger';

    private $message;

    public function __construct(string $message)
    {
        $this->message = $message . " at: " . date(DATE_ATOM);
    }

    public function getRecipient(): string
    {
        return 'bitrix_site';
    }

    public function getContent(): string
    {
        return $this->message;
    }

    public function send(): ResultInterface
    {
        $result = \CAdminNotify::add([
            "MESSAGE" => $this->message,
            "TAG" => static::class,
            "MODULE_ID" => "winter.main",
            "ENABLE_CLOSE" => "Y",
            "PUBLIC_SECTION" => "N",
        ]);

        if (!$result) {
            return Result::error(I18n::getMessage(self::CHANNEL, 'messenger_send_error'))
                ->addTags('admin_informer')
                ->addTags('system')
            ;
        }

        return Result::success()
            ->addTags('admin_informer')
            ->addTags('system')
        ;
    }
}