<?php


namespace Winter\Main\Messengers;


use Bitrix\Main\Mail\Event;
use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Logger;
use Winter\Main\Result;

class BitrixEmailMessenger implements MessengerWithContext
{
    const CHANNEL = 'messenger';

    private $email;
    private $name;
    private $replace;
    /**
     * @var null
     */
    private $language;

    public function __construct(string $email, string $name, array $replace=[])
    {
        $this->email = $email;
        $this->name = $name;
        $this->replace = $replace;
        $this->language = CUSTOM_LANGUAGE;
    }

    public function getRecipient(): string
    {
        return $this->email;
    }

    public function getContent(): string
    {
        return  $this->name;
    }

    public function setLanguage(string $lang):self
    {
        if (!in_array($lang, LANGS_LIST)) {
            return $this;
        }

        $this->language = $lang;

        return $this;
    }

    public function send(): ResultInterface
    {
        $params = [
            "EVENT_NAME" => $this->name,
            "LID" => SITE_ID,
            "LANGUAGE_ID" => $this->language,
            "C_FIELDS" => array_merge($this->replace, ['EMAIL' => $this->email]),
        ];

        Logger::debug(self::CHANNEL, $params, 'Event::send($data)');

        $result = Event::send($params);

        if (!$result->isSuccess()) {
            Logger::error(self::CHANNEL, $result->getErrorMessages(), 'Event::send');
            return Result::error(I18n::getMessage(self::CHANNEL, 'messenger_email_send_error'))
                ->addTags('email')
            ;
        }

        return Result::success()
            ->addTags('email')
        ;
    }
}