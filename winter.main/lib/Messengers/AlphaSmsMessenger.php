<?php


namespace Winter\Main\Messengers;


use Winter\Main\I18n;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Logger;
use Winter\Main\Messengers\Entities\SentMessage;
use Winter\Main\Messengers\Settings\AlphaSmsSettings;
use Winter\Main\Messengers\Settings\TurboSmsSettings;
use Winter\Main\Result;
use Winter\Main\Services\AuthorizedHttpClient;

class AlphaSmsMessenger implements MessengerWithContext
{
    const CHANNEL = 'messenger';

    private $settings;
    private $phone;
    private $text;

    public function __construct(string $phone, string $text)
    {
        $this->settings = new AlphaSmsSettings();
        $this->phone = $phone;
        $this->text = $text;
    }

    public function getRecipient(): string
    {
        return $this->phone;
    }

    public function getContent(): string
    {
        return $this->text;
    }

    public function initSend()
    {
        if (!$this->settings->getToken()) {
            return [
                'errors' => ['BITRIX_EMPTY_TOKEN'],
            ];
        }
        if (!$this->settings->getSmsSender()) {
            return [
                'errors' => ['BITRIX_EMPTY_SENDER'],
            ];
        }
        $data = [
            "version" => "http",
            "command" => "send",
            "key" => $this->settings->getToken(),
            "from" => $this->settings->getSmsSender(),
            "to" => $this->phone,
            "message" => $this->text,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, AlphaSmsSettings::ENDPOINT . '?' . http_build_query($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);

        curl_close($ch);

        $response = trim($response);

        Logger::debug(self::CHANNEL, $response);

        $result = [
            'id' => '',
            'errors' => [$response],
        ];

        if (strpos($response, 'id:') === 0 && preg_match('@id\:([0-9]+)@si', $response, $request_arr)) {
            $result['id'] = (int) $request_arr[1];
            $result['errors'] = null;
        }

        if (strpos($response, 'errors:') === 0) {
            $result['errors'] = array_filter(explode('errors:', $response), 'strlen');
        }

        return $result;
    }

    public function send(): ResultInterface
    {
        $smsResult = $this->initSend();

        $smsId = $smsResult['id'];

        if ($smsId) {
            return Result::success()
                ->set('id', $smsId)
                ->addTags('alpha_sms')
                ->addTags('sms_send')
                ->addTags('has_id')
            ;
        }

        Logger::error(self::CHANNEL, compact('smsResult'));

        return Result::error()
            ->addTags('alpha_sms')
            ->addTags('sms_send')
        ;
    }
}