<?php


namespace Winter\Main\Messengers\Entities;


use Winter\Main\Entities\SentMessagesTable;
use Winter\Main\I18n;
use Winter\Main\Interfaces\CouldBeSaved;
use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Logger;
use Winter\Main\Result;

class SentMessage implements CouldBeSaved
{
    const CHANNEL = 'entity';

    private $id;
    private $type;
    private $messenger;
    private $entity_id;
    private $target_id;
    private $recipient;
    private $language;
    private $content;

    public function __construct()
    {
        $this->id = '';
        $this->type = '';
        $this->messenger = '';
        $this->entity_id = '';
        $this->target_id = '';
        $this->recipient = '';
        $this->language = '';
        $this->content = '';
    }

    public function setId($id): SentMessage
    {
        $this->id = $id;
        return $this;
    }

    public function setType($type): SentMessage
    {
        $this->type = $type;
        return $this;
    }

    public function setMessenger($messenger): SentMessage
    {
        $this->messenger = $messenger;
        return $this;
    }

    public function setEntityId($entity_id): SentMessage
    {
        $this->entity_id = $entity_id;
        return $this;
    }

    public function setTargetId($target_id): SentMessage
    {
        $this->target_id = $target_id;
        return $this;
    }

    public function setRecipient($recipient): SentMessage
    {
        $this->recipient = $recipient;
        return $this;
    }

    public function setLanguage($language): SentMessage
    {
        $this->language = $language;
        return $this;
    }

    public function setContent($content): SentMessage
    {
        $this->content = $content;
        return $this;
    }

    public function save(): ResultInterface
    {
        $data = [
            "external_id" => $this->id,
            "type" => $this->type,
            "messenger" => $this->messenger,
            "entity_id" => $this->entity_id,
            "target_id" => $this->target_id,
            "recipient" => $this->recipient,
            "language" => $this->language,
            "content" => $this->content,
        ];

        $result = SentMessagesTable::add($data);

        if (!$result->isSuccess()) {
            Logger::error(self::CHANNEL, $result->getErrorMessages(), 'SentMessagesTable::add');
            return Result::error(I18n::getMessage(self::CHANNEL, 'entity_sent_message_not_saved'));
        }

        return Result::success(I18n::getMessage(self::CHANNEL, 'entity_sent_message_saved'));
    }
}