<?php


namespace Winter\Main;


class Cache
{
    const CHANNEL = 'cache';

    public static function remember(string $code, int $time, callable $callback)
    {
        $result = null;
        $isValid = false;
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        $dir = 'limited';
        if ($cache->initCache($time, $code, 'limited'))
        {
            $isValid = true;
            $result = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($dir);
            $isValid = true;
            $result = $callback();
            $CACHE_MANAGER->RegisterTag($code);
            $CACHE_MANAGER->EndTagCache();
            $cache->endDataCache($result);
        }

        if (!$isValid) {
            Logger::error(self::CHANNEL, "dir:$dir code:$code", "Cache::remember");
        }

        return $result;
    }

    public static function clear(string $code)
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag($code);
    }
}