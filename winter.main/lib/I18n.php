<?php


namespace Winter\Main;


use Winter\Main\Localization\BitrixLocalization;
use Winter\Main\Localization\DBLocalization;
use Winter\Main\Localization\I18nInterface;
use Winter\Main\Localization\SystemLocalization;

class I18n implements I18nInterface
{
    public static function getCountableMessage($code, int $amount): string
    {
        $code .= '_'.self::declOfNum($amount);

        return self::getMessage('countable', $code, ['#AMOUNT#' => $amount]);
    }

    public static function getMessage($group, $code, array $replace=null): string
    {
        if (strpos($group, $code) !== 0) {
            Logger::debug('localization', $code, $group);
        }
        if (strpos($group, 'db:') === 0) {
            return DBLocalization::getMessage(substr($group, 3), $code, $replace);
        }
        return BitrixLocalization::getMessage($group, $code, $replace) ?: $group.':'.$code;
    }

    private static function declOfNum($number)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
        return (($number%100>4 && $number%100<20) ? 2 : $cases[min($number%10, 5)]);
    }

    public static function url(string $url): string
    {
        $lang = LANGUAGE_ID;
        if (DEFAULT_LANGUAGE === LANGUAGE_ID
            || strpos($url, "/$lang/") === 0) {
            return $url;
        }

        $url = ltrim($url, '/');

        return "/$lang/$url";
    }

    public static function system($code, array $replace=null)
    {
        return SystemLocalization::getMessage('system', $code, $replace);
    }
}