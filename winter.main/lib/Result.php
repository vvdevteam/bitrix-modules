<?php


namespace Winter\Main;


use Winter\Main\Interfaces\ResultInterface;

class Result implements ResultInterface
{
    private $success;
    private $messages;
    private $tags;
    private $data;

    public static function success(string $message=''): ResultInterface
    {
        return (new static(true))
            ->addMessage($message)
            ;
    }

    public static function error(string $message=''): ResultInterface
    {
        return (new static(false))
            ->addMessage($message)
            ;
    }

    private function __construct(bool $success)
    {
        $this->success = $success;
        $this->messages = [];
        $this->tags = [];
        $this->data = [];
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function addMessage(string $message): ResultInterface
    {
        if (!empty($message)) {
            $this->messages[] = $message;
        }

        return $this;
    }

    public function addTags(string $tag): ResultInterface
    {
        if (!empty($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function getTags(): array
    {
        return  $this->tags;
    }

    public function get(string $code)
    {
        return $this->data[$code] ?? '';
    }

    public function set(string $code, $value): ResultInterface
    {
        $this->data[$code] = $value;

        return $this;
    }

    public function toArray()
    {
        return [
            'isSuccess' => $this->success,
            'messages' => $this->messages,
            'tags' => $this->tags,
            'data' => $this->data,
        ];
    }
}