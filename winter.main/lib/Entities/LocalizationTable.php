<?php


namespace Winter\Main\Entities;


use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;

class LocalizationTable extends LoggableDataManager
{
    public static function getTableName()
    {
        return 'b_localization';
    }

    public static function getMap(): array
    {
        return [
            (new IntegerField('id'))
                ->configurePrimary(true)
                ->configureAutocomplete(true),
            new StringField('type'),
            new StringField('code'),
            new StringField('language'),
            new StringField('value'),
        ];
    }

    public static function import()
    {
        return <<<SQL
    CREATE TABLE IF NOT EXISTS b_localization (
        id int(11) AUTO_INCREMENT PRIMARY KEY,
        type varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        code varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        language char(2) COLLATE utf8_unicode_ci NOT NULL,
        value text COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
    }
}