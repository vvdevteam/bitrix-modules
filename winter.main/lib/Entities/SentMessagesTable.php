<?php


namespace Winter\Main\Entities;


use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;

class SentMessagesTable extends LoggableDataManager
{
    public static function getTableName()
    {
        return 'b_sent_messages';
    }

    public static function getMap(): array
    {
        return [
            (new IntegerField('id'))
                ->configurePrimary(true)
                ->configureAutocomplete(true),
            new StringField('external_id'),
            new StringField('type'),
            new StringField('entity_id'),
            new IntegerField('target_id'),
            new StringField('recipient'),
            new StringField('messenger'),
            (new BooleanField('is_delivered'))
                ->configureDefaultValue(0)
            ,
            (new BooleanField('is_old'))
                ->configureDefaultValue(0)
            ,
            new StringField('language'),
            new StringField('content'),
        ];
    }

    public static function import()
    {
        return <<<SQL
    CREATE TABLE IF NOT EXISTS b_sent_messages (
        id int(11) AUTO_INCREMENT PRIMARY KEY,
        type varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        messenger varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        external_id varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        entity_id varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        target_id int(11) NOT NULL,
        is_delivered tinyint(1) NOT NULL,
        is_old tinyint(1) NOT NULL,
        language char(2) COLLATE utf8_unicode_ci NOT NULL,
        recipient varchar(72) COLLATE utf8_unicode_ci NOT NULL,
        content text  COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
    }
}