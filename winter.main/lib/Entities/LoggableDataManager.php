<?php


namespace Winter\Main\Entities;


use Bitrix\Main\ORM\Data\DataManager;
use Winter\Main\Logger;

abstract class LoggableDataManager extends DataManager
{
    const CHANNEL = 'orm';

    public static function add(array $data)
    {
        Logger::debug(static::CHANNEL, $data, 'add');
        return parent::add($data);
    }

    public static function update($primary, array $data)
    {
        Logger::debug(static::CHANNEL, [$primary, $data], 'update');
        return parent::update($primary, $data);
    }

    public static function delete($primary)
    {
        Logger::debug(static::CHANNEL, $primary, 'delete');
        return parent::delete($primary);
    }

    public static function getQueryClass()
    {
        return ExtendedQuery::class;
    }

    /**
     * Creates and returns the Query object for the entity
     *
     * @return ExtendedQuery
     */
    public static function query()
    {
        return parent::query();
    }
}