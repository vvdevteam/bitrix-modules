<?php


namespace Winter\Main\Entities\HighLoad;


use Winter\Main\Logger;

abstract class AbstractHighLoadDataManager
{
    const CHANNEL = 'high_load_block_orm';

    protected abstract static function getName(): string;

    public static function getDataClass()
    {
        return HighLoadBlockEntity::getDataClass(static::getName());
    }

    public static function query()
    {
        $className = static::getDataClass();

        return $className::query();
    }

    public static function add(array $data)
    {
        Logger::debug(static::CHANNEL, $data, 'add');

        $className = static::getDataClass();

        return $className::add($data);
    }

    public static function update($primary, array $data)
    {
        Logger::debug(static::CHANNEL, [$primary, $data], 'update');

        $className = static::getDataClass();

        return $className::update($primary, $data);
    }

    public static function delete($primary)
    {
        Logger::debug(static::CHANNEL, $primary, 'delete');

        $className = static::getDataClass();

        return $className::delete($primary);
    }


}