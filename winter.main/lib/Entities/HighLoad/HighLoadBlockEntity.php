<?php


namespace Winter\Main\Entities\HighLoad;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ORM\Data\DataManager;

class HighLoadBlockEntity
{
    private static $entities = [];

    public static function getDataClass(string $name)
    {
        if (!empty(self::$entities[$name])) {
            return self::$entities[$name];
        }

        $hl = HighloadBlockTable::query()
            ->where('NAME', $name)
            ->addSelect('*')
            ->exec()
            ->fetch()
        ;
        if (empty($hl)) {
            throw new \Exception("$name is not exist");
        }

        return self::$entities[$name] = HighloadBlockTable::compileEntity($hl)->getDataClass();
    }
}