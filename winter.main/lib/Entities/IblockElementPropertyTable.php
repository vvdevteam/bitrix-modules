<?php


namespace Winter\Main\Entities;


use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ORM\Fields\FloatField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Query\Join;

class IblockElementPropertyTable extends LoggableDataManager
{
    public static function getTableName()
    {
        return 'b_iblock_element_property';
    }

    public static function getMap()
    {
        return [
            (new IntegerField('ID'))
                ->configurePrimary(true),
            (new IntegerField('IBLOCK_PROPERTY_ID'))
                ->configureRequired(true),
            (new IntegerField('IBLOCK_ELEMENT_ID'))
                ->configureRequired(true),
            new StringField('VALUE'),
            (new StringField('VALUE_TYPE'))
                ->configureSize(4),
            new IntegerField('VALUE_ENUM'),
            (new FloatField('VALUE_NUM'))
                ->configureScale(2),
            (new StringField('DESCRIPTION'))
                ->configureSize(255),
            new ReferenceField('IBLOCK_PROPERTY',
                PropertyTable::class,
                Join::on('this.IBLOCK_PROPERTY_ID', 'ref.ID')),
            new ReferenceField('IBLOCK_ELEMENT',
                ElementTable::class,
                Join::on('this.IBLOCK_ELEMENT_ID', 'ref.ID')),
        ];
    }

    public static function getForIblockElementReference(string $code, int $propId) {
        return new ReferenceField(
            $code,
            static::class,
            Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                ->where('ref.IBLOCK_PROPERTY_ID', $propId)
        );
    }
}