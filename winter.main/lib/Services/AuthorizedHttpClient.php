<?php


namespace Winter\Main\Services;


class AuthorizedHttpClient
{
    private $token;

    public function __construct(string $token) {
        $this->token = $token;
    }

    public function isReady() {
        return strlen($this->token) > 0;
    }

    public function post($url, $data=[]) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Bearer '. $this->token,
        ));
        $server_output = curl_exec($ch);
        curl_close ($ch);
        return json_decode($server_output, true);
    }
}