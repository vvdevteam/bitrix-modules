<?php


namespace Winter\Main;


class Image
{
    public static function getPath(array $idList, array $size=[])
    {
        if (!empty($size)) {
            list($width, $height) = $size;
        }
        $pathList = [];
        foreach ($idList as $id) {
            $pathList[] = \CFile::GetPath($id);
        }
        return $pathList;
    }
}