<?php


namespace Winter\Main\Users;


use Winter\Main\Interfaces\ResultInterface;
use Winter\Main\Result;

class User
{
    private $id;

    private static $user;

    //convert to host int $id
    //convert to client int $id
    //close user int $id

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function current(): self
    {
        if (empty(self::$user)) {
            global $USER;
            self::$user = new self((int) $USER->GetID());
        }

        return self::$user;
    }

    public function close(): ResultInterface
    {
        //добавити перевірку на активні замовлення
        //BookingTable::update('active' => 0)
        return Result::success();
    }

    public function isVerified()
    {
        return !!$this->id;
    }

    public function isHost()
    {
        return !!$this->id;
    }
}