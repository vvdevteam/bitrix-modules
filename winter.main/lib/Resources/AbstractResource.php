<?php


namespace Winter\Main\Resources;


use Bitrix\Main\ORM\Query\Query;

abstract class AbstractResource
{
    protected $query;

    public static function instance()
    {
        return new static();
    }

    abstract protected function init();

    public function query(): Query
    {
        return $this->query;
    }
}