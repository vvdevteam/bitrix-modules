<?php


namespace Winter\Main;


class Session
{
    const CHANNEL = 'session';

    public static function remember(string $id, int $time, callable $callback)
    {
        $code = $id;
        if (strpos($id, ':') !== false) {
            list($id, $code) = explode($id, ':', 2);
        }
        $cache = null;
        if (
            isset($_SESSION[$id])
            && is_array($_SESSION[$id])
            && $_SESSION[$id]['code'] === $code
            && $_SESSION[$id]['time'] + $time >= microtime(1)
        ) {
            $cache = $_SESSION[$id];
        } else {
            $cache = $_SESSION[$id] = [
                'code' => $code,
                'data' => $callback(),
                'time' => (int) microtime(1)
            ];
        }

        return !empty($cache) ? $cache['data'] : null;
    }

    public static function clear(string $id)
    {
        $_SESSION[$id] = null;
    }
}