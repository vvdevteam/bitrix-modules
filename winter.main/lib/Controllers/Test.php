<?php


namespace Winter\Main\Controllers;


use Bitrix\Main\Application;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Engine\Controller;
use Winter\Main\I18n;

class Test extends Controller
{
    public function configureActions()
    {
        return [
            'ping' => [
                'prefilters' => [
                    new HttpMethod([HttpMethod::METHOD_POST]),
                    new Csrf(),
                ],
                'postfilters' => [],
            ],
            'fileSave' => [
                'prefilters' => [
                    new HttpMethod([HttpMethod::METHOD_POST]),
                    new Csrf(),
                ],
                'postfilters' => [],
            ],
        ];
    }

    public function pingAction() {
        return [
            'message' => I18n::getMessage('test', 'test'),
        ];
    }

    public function fileSaveAction() {

        $request = Application::getInstance()->getContext()->getRequest();

        return [
            'files' => $_FILES,
            'message' => I18n::getMessage('test', 'test'),
        ];
    }
}