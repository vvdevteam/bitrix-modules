<?php


namespace Winter\Main;


use Bitrix\Main\Diag\Debug;

class Logger
{
    const CHANNELS = [
        'system'
    ];

    public static function debug(string $channel, $data, string $action='') {
        Debug::writeToFile($data, $action, 'local/logs/debug_'.$channel. '_' . date('Y-m') . '.log');
    }

    public static function error(string $channel, $data, string $action='') {
        Debug::writeToFile($data, $action, 'local/logs/'.$channel . '_' . date('Y-m') .'.log');
    }

    public static function query($queryString)
    {

    }
}