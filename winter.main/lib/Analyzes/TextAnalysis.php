<?php


namespace Winter\Main\Analyzes;


class TextAnalysis
{
    const CHANNEL = 'analysis';

    private $elements;
    private $userId;

    public function __construct(int $userId)
    {
        $this->elements = [];
        $this->userId = $userId;
    }

    public function add(string $text)
    {
        $this->elements[] = $text;

        return $this;
    }

    private function handleElement(string $element): AnalysisResult
    {
        $result = new AnalysisResult();

        $textWithoutHtml = strip_tags($element);

        $textArray = preg_split('@\s+@', $textWithoutHtml);

        $numberAmount = 0;
        $phoneAmount = 0;
        $numberThings = [];
        $wordThings = [];
        $allNumbers = [];
        foreach ($textArray as $i => $word) {
            $currentWord = trim($word);
            $number = null;

            if(Detector::isMessenger($currentWord)) {
                $result->addSuspicious("messenger:$currentWord");
            }
            if(Detector::isUrl($currentWord)) {
                $result->addSuspicious("url:$currentWord");
                continue;
            }
            if(Detector::isSomeLogin($currentWord)) {
                $result->addSuspicious("login:$currentWord");
                continue;
            }
            if (Detector::hasNumber($currentWord)) {
                $numberAmount++;
                $number = preg_replace("@\D+@", "", $currentWord);
                $allNumbers[] = $number;
                $numberThings[] = $number;
                $wordThings[] = $currentWord;
                $isLineOfNumbers = true;
            } else  {
                $isLineOfNumbers = false;
            }
            if (!empty($number) && Detector::isPhone($number)) {
                $phoneAmount++;
                $result->addProhibited("phone:$number original:$currentWord");
                continue;
            }
            if (!empty($number)) {
                $supposedPhone = implode('', $allNumbers);
                if (Detector::isPhone($supposedPhone)) {
                    $result->addSuspicious("fragmented phone:$supposedPhone");
                }
            }
            if (!$isLineOfNumbers && !empty($numberThings)) {
                $supposedPhone = implode('', $numberThings);
                $supposedPhoneWord = implode(' ', $wordThings);
                if (Detector::isPhone($supposedPhone)) {
                    $phoneAmount++;
                    $result->addProhibited("phone:$supposedPhone original:$supposedPhoneWord");
                }
                $wordThings = [];
                $numberThings = [];
            }

        }

        if ($numberAmount > 1 && !$phoneAmount) {
            $result->addSuspicious("contain suspicious numbers");
        }

        return $result;
    }

    public function start()
    {
        foreach ($this->elements as $element) {
            $result = $this->handleElement($element);
            pre($result);
        }
    }

    public function clear()
    {
        $this->elements = [];

        return $this;
    }
}