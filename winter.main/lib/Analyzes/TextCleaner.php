<?php


namespace Winter\Main\Analyzes;


class TextCleaner
{
    private $result;

    public function __construct()
    {
        $this->result = new AnalysisResult();
    }

    public function clean($text)
    {
        $cleanedText = '';

        $textWithoutHtml = strip_tags($text);

        $textArray = preg_split('@\s+@', $textWithoutHtml);

        $numberAmount = 0;
        $phoneAmount = 0;
        $numberThings = [];
        $numberThingsIndexes = [];
        $wordThings = [];
        $allNumbers = [];
        foreach ($textArray as $i => $word) {
            $currentWord = trim($word);
            $number = null;

            if(Detector::isMessenger($currentWord)) {
                $this->result->addSuspicious("messenger:$currentWord");
            }
            if(Detector::isUrl($currentWord)) {
                $this->result->addSuspicious("url:$currentWord");
                continue;
            }
            if(Detector::isSomeLogin($currentWord)) {
                $this->result->addSuspicious("login:$currentWord");
                continue;
            }
            if (Detector::hasNumber($currentWord)) {
                $numberAmount++;
                $number = preg_replace("@\D+@", "", $currentWord);
                $isLineOfNumbers = true;
            } else  {
                $isLineOfNumbers = false;
            }
            if (!empty($number) && Detector::isPhone($number)) {
                $phoneAmount++;
                $this->result->addProhibited("phone:$number original:$currentWord");
                unset($textArray[$i]);
                continue;
            }
            if (!empty($number)) {
                $allNumbers[] = $number;
                $numberThings[] = $number;
                $numberThingsIndexes[] = $i;
                $wordThings[] = $currentWord;
            }
            if (!empty($number)) {
                $supposedPhone = implode('', $allNumbers);
                if (Detector::isPhone($supposedPhone)) {
                    $this->result->addSuspicious("fragmented phone:$supposedPhone");
                }
            }
            if (!$isLineOfNumbers && !empty($numberThings)) {
                $supposedPhone = implode('', $numberThings);
                $supposedPhoneWord = implode(' ', $wordThings);
                if (Detector::isPhone($supposedPhone)) {

                    $phoneAmount++;
                    $this->result->addProhibited("phone:$supposedPhone original:$supposedPhoneWord");
                    foreach ($numberThingsIndexes as $numberThingsIndex) {
                        unset($textArray[$numberThingsIndex]);
                    }
                }
                $wordThings = [];
                $numberThings = [];
                $numberThingsIndexes = [];
            }

        }

        if ($numberAmount > 1 && !$phoneAmount) {
            $this->result->addSuspicious("contain suspicious numbers");
        }

        $cleanedText = implode(' ', array_filter($textArray, 'trim'));
        
        return $cleanedText;
    }

    public function getResult(): AnalysisResult
    {
        return $this->result;
    }
}