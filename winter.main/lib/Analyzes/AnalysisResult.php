<?php


namespace Winter\Main\Analyzes;


class AnalysisResult
{
    private $prohibited;
    private $suspicious;
    private $undefined;
    private $level;

    public function __construct()
    {
        $this->prohibited = [];
        $this->suspicious = [];
        $this->undefined = [];
        $this->level = 0;
    }

    public function getProhibited(): array
    {
        return $this->prohibited;
    }

    public function addProhibited(string $prohibited): self
    {
        $this->prohibited[] = $prohibited;

        if ($this->level < 3) {
            $this->level = 3;
        }

        return $this;
    }

    public function getSuspicious(): array
    {
        return $this->suspicious;
    }

    public function addSuspicious(string $suspicious): self
    {
        $this->suspicious[] = $suspicious;

        if ($this->level < 2) {
            $this->level = 2;
        }

        return $this;
    }

    public function getUndefined(): array
    {
        return $this->undefined;
    }

    public function addUndefined(string $undefined): self
    {
        $this->undefined[] = $undefined;

        if ($this->level < 1) {
            $this->level = 1;
        }

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }
}