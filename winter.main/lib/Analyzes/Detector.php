<?php


namespace Winter\Main\Analyzes;


class Detector
{
    public static function hasNumber(string $text)
    {
        return preg_match("@\d+@", $text);
    }

    public static function isPhone($text)
    {
        return preg_match("@^\+?3?8?[0]?[1-9]{1}\d{8}$@", $text);
    }

    public static function isUrl($text)
    {
        return preg_match("@^(?:https?:\/\/)?(?:[A-z0-9]{1}|\/\/[A-z0-9])[A-z0-9.-_]+\.[A-z0-9.-_]+@", $text);
    }

    public static function isSomeLogin($text)
    {
        return preg_match("/@{1}/", $text);
    }

    public static function isMessenger($text)
    {
        return in_array($text, [
            'телега',
            'телеграм',
            'тг',
            'вайбер',
        ]);
    }
}