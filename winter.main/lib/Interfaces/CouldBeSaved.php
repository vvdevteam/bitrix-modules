<?php


namespace Winter\Main\Interfaces;


interface CouldBeSaved
{
    public function save(): ResultInterface;
}