<?php


namespace Winter\Main\Interfaces;


interface CouldBeBought
{
    public function addToBasket(int $id): self;
}