<?php


namespace Winter\Main\Interfaces;


interface ResultInterface
{
    public function isSuccess(): bool;

    public function getMessages(): array;

    public function addMessage(string $message): self;

    public function addTags(string $tag): self;

    public function getTags(): array;

    public function get(string $code);

    public function set(string $code, $value): self;
}