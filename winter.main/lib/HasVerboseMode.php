<?php


namespace Winter\Main;


trait HasVerboseMode
{
    private $verboseMode = false;

    public function enableVerboseMode()
    {
        $this->verboseMode = true;

        return $this;
    }

    public function disableVerboseMode()
    {
        $this->verboseMode = false;

        return $this;
    }

    public function isEnabledVerboseDebug()
    {
        return $this->verboseMode;
    }
}