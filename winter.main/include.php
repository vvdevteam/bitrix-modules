<?
function chunk(\Bitrix\Main\ORM\Query\Query $query, int $limitForStep): \Generator
{
    $isNext = true;
    $step = 0;
    while($isNext) {
        $query->setLimit($limitForStep)
            ->setOffset($limitForStep*$step)
        ;
        $result = $query->exec();
        $isNext = false;
        $step++;
        while($item = $result->fetch()) {
            $isNext = true;
            yield $item;
        }
    }
}