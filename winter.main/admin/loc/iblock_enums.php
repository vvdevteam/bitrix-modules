<?php

use Bitrix\Iblock\PropertyEnumerationTable;
use Winter\Main\Entities\LocalizationTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/prolog.php");

//if(!$USER->CanDoOperation('view_other_settings'))
    //$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
\Bitrix\Main\Loader::includeModule('winter.main');
function pre($value)
{
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}
$enumProps = \Bitrix\Iblock\PropertyTable::query()
    ->whereNull('USER_TYPE')
    ->where('PROPERTY_TYPE', 'L')
    ->where('IBLOCK_ID', CARS_IBLOCK_ID)
    ->addSelect('ID')
    ->addSelect('NAME')
    ->exec()
    ->fetchAll()
;
//content start?>
    Списки
    <form action="" method="get">
        <select name="property_id">
            <?foreach ($enumProps as $prop){?>
                <option value="<?=$prop['ID']?>"><?=$prop['NAME']?></option>
            <?}?>
        </select>
        <button>Сохранить</button>
    </form>
<br/>
<?
if (!$_GET['property_id']) {
    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
    die;
}
$savedLocs = LocalizationTable::query()
    ->where('type', 'cars:settings')
    ->addSelect('code')
    ->addSelect('value')
    ->addSelect('language')
    ->addSelect('id')
    ->exec()
    ->fetchAll()
;
$savedLocs = array_map(function ($item) {
    list(, $id) = explode(':', $item['code']);
    $item['key'] = "enum:$id:" . $item['language'];
    return $item;
}, $savedLocs);
$savedValues = array_column($savedLocs, 'value', 'key');
$savedLocs = array_column($savedLocs, 'id', 'key');
$enums = PropertyEnumerationTable::query()
    ->where('PROPERTY_ID', $_GET['property_id'])
    ->addSelect('ID')
    ->addSelect('VALUE', 'NAME')
    ->exec()
    ->fetchAll()
;
?>
    <form action="" method="post">
        <input type="hidden" name="property_id" value="<?=$_GET['property_id']?>">
        <table>
            <tr>
                <td>default</td>
                <?foreach (LANGS_LIST as $lang){?>
                    <td> <?=$lang?></td>
                <?}?>
            </tr>
        <?foreach ($enums as $enum){?>
            <tr>
                <td><span><?=$enum['NAME']?></span></td>
                <?foreach (LANGS_LIST as $lang){
                    $value = $savedValues["enum:".$enum['ID'].":$lang"] ?? '';
                    if (!$value && DEFAULT_LANGUAGE == $lang) {
                        $value = $enum['NAME'];
                    }
                    ?>
                    <td>
                        <input type="text" name="enums[enum:<?=$enum['ID']?>:<?=$lang?>]" value="<?=$value?>">
                    </td>
                <?}?>
            </tr>
        <?}?>
        </table>
        <button>Сохранить</button>
    </form>
<?//content end
if (empty($_POST)) {
    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
    die;
}

foreach ($_POST['enums'] as $key => $value) {
    list(,$id,$lang) = explode(':', $key);
    $locId = $savedLocs["enum:$id:$lang"] ?? 0;
    $data = [
        'type' => "cars:settings",
        'code' => "enum:$id:VALUE",
        'language' => $lang,
        'value' => $value,
    ];
    if ($locId) {
        LocalizationTable::update($locId, ['value' => $value]);
    } else {
        LocalizationTable::add($data);
    }
}
LocalRedirect($_SERVER['REQUEST_URI']);
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");