<?php

use Bitrix\Iblock\PropertyEnumerationTable;
use Winter\Main\Entities\LocalizationTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/prolog.php");

//if(!$USER->CanDoOperation('view_other_settings'))
    //$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
\Bitrix\Main\Loader::includeModule('winter.main');
function pre($value)
{
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}
$props = \Bitrix\Iblock\PropertyTable::query()
    ->whereNull('USER_TYPE')
    ->where('IBLOCK_ID', CARS_IBLOCK_ID)
    ->where('ACTIVE', 'Y')
    ->addSelect('ID')
    ->addSelect('NAME')
    ->exec()
    ->fetchAll()
;
//content start
$savedLocs = LocalizationTable::query()
    ->where('type', 'cars:settings')
    ->addSelect('code')
    ->addSelect('value')
    ->addSelect('language')
    ->addSelect('id')
    ->exec()
    ->fetchAll()
;
$savedLocs = array_map(function ($item) {
    list(, $id) = explode(':', $item['code']);
    $item['key'] = "property:$id:" . $item['language'];
    return $item;
}, $savedLocs);
$savedValues = array_column($savedLocs, 'value', 'key');
$savedLocs = array_column($savedLocs, 'id', 'key');
;
?>
    <form action="" method="post">
        <table>
            <tr>
                <td>default</td>
                <?foreach (LANGS_LIST as $lang){?>
                    <td> <?=$lang?></td>
                <?}?>
            </tr>
        <?foreach ($props as $prop){?>
            <tr>
                <td><span><?=$prop['NAME']?></span></td>
                <?foreach (LANGS_LIST as $lang){
                    $value = $savedValues["property:".$prop['ID'].":$lang"] ?? '';
                    if (!$value && DEFAULT_LANGUAGE == $lang) {
                        $value = $prop['NAME'];
                    }
                    ?>
                    <td>
                        <input type="text" name="properties[property:<?=$prop['ID']?>:<?=$lang?>]" value="<?=$value?>">
                    </td>
                <?}?>
            </tr>
        <?}?>
        </table>
        <button>Сохранить</button>
    </form>
<?//content end
if (empty($_POST)) {
    require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
    die;
}
foreach ($_POST['properties'] as $key => $value) {
    if (!$value) {
        continue;
    }
    list(,$id,$lang) = explode(':', $key);
    $locId = $savedLocs["property:$id:$lang"] ?? 0;
    $data = [
        'type' => "cars:settings",
        'code' => "property:$id:NAME",
        'language' => $lang,
        'value' => $value,
    ];
    if ($locId) {
        LocalizationTable::update($locId, ['value' => $value]);
    } else {
        LocalizationTable::add($data);
    }
}
LocalRedirect($_SERVER['REQUEST_URI']);
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");