<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;


$moduleId = 'winter.main';

Loc ::loadMessages( __FILE__);

if ($APPLICATION->GetGroupRight( $moduleId ) < "R" )
{
    $APPLICATION->AuthForm(Loc ::getMessage( "ACCESS_DENIED" ) );
}

Loader ::includeModule( $moduleId );

$aTabs = [
    [
        'DIV' => 'notification',
        'TAB' => 'Sms',
        'TITLE' => 'Sms',
        'OPTIONS' => [
            [
                'TURBO_SMS_SMS_SENDER',
                'TURBO_SMS_SMS_SENDER',
                '',
                ['text', 75]

            ],
            [
                'TURBO_SMS_TOKEN',
                'TURBO_SMS_TOKEN',
                '',
                ['text', 75]

            ],
            [
                'ALPHA_SMS_SMS_SENDER',
                'ALPHA_SMS_SMS_SENDER',
                '',
                ['text', 75]

            ],
            [
                'ALPHA_SMS_TOKEN',
                'ALPHA_SMS_TOKEN',
                '',
                ['text', 75]

            ],
        ]
    ],

];

if ($request->isPost() && check_bitrix_sessid()) {
    if (strlen($request[ 'save' ]) > 0) {
        foreach ( $aTabs as $arTab ) {
            if($arTab["TYPE"] != 'rights')
            {
                __AdmSettingsSaveOptions( $moduleId, $arTab['OPTIONS']);
            }
        }
    }
}
$tabControl = new CAdminTabControl( 'tabControl', $aTabs );
$realModuleId = $moduleId;
?>
<form method='post' action='<? echo $APPLICATION -> GetCurPage() ?>?mid=<?= $moduleId ?>&amp;lang=<?= $request[ 'lang' ] ?>' name='<?= $moduleId ?>_settings'>
    <?
    $tabControl->Begin();
    foreach ( $aTabs as $aTab ) {
        $tabControl->BeginNextTab();
        if ($aTab['OPTIONS']) {
            __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
        }
    }
    echo bitrix_sessid_post();
    $tabControl -> Buttons( array( 'btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false, "btnSave" => true ) );
    $tabControl -> End(); ?>
    <?//need for tab_rights. If in $_REQUEST hasn't Update -> rights do not save?>
    <input type="hidden" name="Update" value="Y" />
</form>