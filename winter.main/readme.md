Winter\Main\Analyzes
аналіз і очистка тексту

Winter\Main\Controllers
бекенд для аякс запитів
приклади запитів на js тут local/modules/winter.main/script.js.example

Winter\Main\Entities
орм класи віднаслідувані від Bitrix\Main\ORM\Data\DataManager
але всі нові створювати від Winter\Main\EntitiesLoggableDataManager

Winter\Main\Events
всі бітріксові події реєструвати тут

Winter\Main\Localization
все що стосується локалізації

Winter\Main\Messengers
відправка повідомлень смс, email і ін.
всі нові мають мати інтерфейс Winter\Main\Messengers\MessengerInterface
AlphaSmsMessenger
TurboSmsMessenger
BitrixEmailMessenger
BitrixAdminInformMessenger (зелені плашки під адміном)
MessengerCollection (відправлення з запасними варіантами і збереження в таблицю тих що мають message_id)

Winter\Main\Notifications
відпрвка на конткретну подію
приклад EmergencyAdminNotification

Winter\Main\Reporters
виконання певної дії і надсилання звітів по цій дії

Winter\Main\Resources
запити на д7

Winter\Main\Services
сервіси


Winter\Main\Cache
головна обгортка якщо потрібно буде закешувати

Winter\Main\Session
збережження в сесії

Winter\Main\I18n
головна обгортка при виводі локалізованих повідомлень

Winter\Main\Image
обгортка для подальшого оптимізованого формування урлів

Winter\Main\Logger
головна обгортка для логера

local/php_interface/init.php
наразі тут корисні константи (CUSTOM_LANGUAGE і ін.)